<?php
abstract class User{
    protected $userName;
    public function setUserName($userName){
        $this->userName = $userName;
    }
    public function getUserName(){
        return $this->userName;
    }
    public  abstract function stateYourRole();

}
class Admin extends User{
    public function stateYourRole(){
        return "Admin";

    }
}
class Viewer extends User{
    public function stateYourRole(){
        return "Viewer";
    }
}

$admin = new Admin();
$admin->setUserName("Balthazar");
echo $admin->getUserName()."<br>";
echo $admin->stateYourRole();